import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** ISO Date */
  DateTime: any;
};


export type Offer = {
  __typename?: 'Offer';
  id: Scalars['Int'];
  ownerId: Scalars['Int'];
  title: Scalars['String'];
  slug: Scalars['String'];
  lat: Scalars['Float'];
  lng: Scalars['Float'];
  radius: Scalars['Float'];
  owner: User;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  email: Scalars['String'];
  name: Scalars['String'];
  createdAt: Scalars['DateTime'];
};

export type Query = {
  __typename?: 'Query';
  users: Array<User>;
  offer: Offer;
  offers: Array<Offer>;
};


export type QueryOfferArgs = {
  offerId: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createUser: User;
  createOffer: Offer;
};


export type MutationCreateUserArgs = {
  email: Scalars['String'];
  name?: Maybe<Scalars['String']>;
};


export type MutationCreateOfferArgs = {
  ownerId: Scalars['Int'];
  title: Scalars['String'];
  slug: Scalars['String'];
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
  radius?: Maybe<Scalars['Float']>;
};

export type UserFragmentFragment = (
  { __typename?: 'User' }
  & Pick<User, 'id' | 'email' | 'name' | 'createdAt'>
);

export type OfferFragmentFragment = (
  { __typename?: 'Offer' }
  & Pick<Offer, 'id' | 'ownerId' | 'title' | 'slug' | 'lat' | 'lng' | 'radius'>
  & { owner: (
    { __typename?: 'User' }
    & UserFragmentFragment
  ) }
);

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = (
  { __typename?: 'Query' }
  & { users: Array<(
    { __typename?: 'User' }
    & UserFragmentFragment
  )> }
);

export type OffersQueryVariables = Exact<{ [key: string]: never; }>;


export type OffersQuery = (
  { __typename?: 'Query' }
  & { offers: Array<(
    { __typename?: 'Offer' }
    & OfferFragmentFragment
  )> }
);

export type OfferQueryVariables = Exact<{
  offerId: Scalars['Int'];
}>;


export type OfferQuery = (
  { __typename?: 'Query' }
  & { offer: (
    { __typename?: 'Offer' }
    & OfferFragmentFragment
  ) }
);

export const UserFragmentFragmentDoc = gql`
    fragment userFragment on User {
  id
  email
  name
  createdAt
}
    `;
export const OfferFragmentFragmentDoc = gql`
    fragment offerFragment on Offer {
  id
  ownerId
  title
  slug
  lat
  lng
  radius
  owner {
    ...userFragment
  }
}
    ${UserFragmentFragmentDoc}`;
export const UsersDocument = gql`
    query users {
  users {
    ...userFragment
  }
}
    ${UserFragmentFragmentDoc}`;
export const OffersDocument = gql`
    query offers {
  offers {
    ...offerFragment
  }
}
    ${OfferFragmentFragmentDoc}`;
export const OfferDocument = gql`
    query offer($offerId: Int!) {
  offer(offerId: $offerId) {
    ...offerFragment
  }
}
    ${OfferFragmentFragmentDoc}`;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    users(variables?: UsersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UsersQuery> {
      return withWrapper(() => client.request<UsersQuery>(UsersDocument, variables, requestHeaders));
    },
    offers(variables?: OffersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<OffersQuery> {
      return withWrapper(() => client.request<OffersQuery>(OffersDocument, variables, requestHeaders));
    },
    offer(variables: OfferQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<OfferQuery> {
      return withWrapper(() => client.request<OfferQuery>(OfferDocument, variables, requestHeaders));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;