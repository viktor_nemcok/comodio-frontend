import { ApolloClient, InMemoryCache } from '@apollo/client';

export const client = new ApolloClient({
  uri: 'https://comodio-graphql-5tth9.ondigitalocean.app/',
  cache: new InMemoryCache()
});