import { createGlobalStyle } from './styled';

const GlobalStyle = createGlobalStyle`
  
  
  *:not(path) {
    font-family: system, -apple-system, ".SFNSText-Regular", "San Francisco", "Roboto", "Segoe UI", "Helvetica Neue", "Lucida Grande", sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    /*all: unset;*/
    display: block;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-text-fill-color: initial;
    outline: 0 !important;
  }

  base,
  basefont,
  datalist,
  head,
  meta,
  script,
  style,
  title,
  noembed,
  param,
  template {
    display: none;
  }

  a {
    display: inline-block;
    color: #5bab8b;
    cursor: pointer;
    
    &:hover {
      color: #5bab8b;
      text-decoration: underline solid #5bab8b;
    }

  }
  pre.CodeMirror-line span {
    display: flex;
  }

  pre.CodeMirror-line span {
    display: flex;
  }


  /* my custom css modifier */
  .ReactModal__Body--open .ril__outer {
    background-color: rgba(0, 0, 0, 0.8);
  }

  .ReactModal__Body--open .ril__toolbar {
    background-color: transparent;
  }

  .ReactModal__Body--open .ril-toolbar-right .ril__toolbarItem {
    display: none;
    background-color: #000;
    border-radius: 100%;
    width: 50px;
    height: 50px;
    text-align: center;
    line-height: 2.3em;
    margin-top: 20px;
  }

  .ReactModal__Body--open .ril-toolbar-right .ril__toolbarItem:last-child {
    display: inline-block;
  }

  @media only screen and (max-width: 1023px) {

    .ReactModal__Body--open .ril-toolbar-right .ril__toolbarItem {
      width: 40px;
      height: 40px;
      line-height: 1.8em;
    }

  }

  .pagination {
    text-align:center;
    margin-top: ${({ theme }) => theme.spacing.big}px;
    margin-bottom: ${({ theme }) => theme.spacing.default}px;
      
      *, *:hover {
        text-decoration: none;
      }

    .item {
      background: ${props => props.theme.colors.pink};
      color: ${props => props.theme.colors.white};
      padding: ${props => props.theme.spacing.small}px ${props => props.theme.spacing.default}px;
      border-radius: ${props => props.theme.border.radius}px;
      margin: 0 ${props => props.theme.spacing.mini}px;

      @media only screen and (max-width: 707px) {
        padding: ${props => props.theme.spacing.mini}px ${props => props.theme.spacing.small}px;
      }

      &:hover {
        opacity: 0.8
      }

      &[aria-current="true"] {
        background: ${props => props.theme.colors.green};
      }
    }

  }

  .hide-on-tablet {
      @media only screen and (max-width: 770px) {
        display: none !important;
      }
    }

    .algolia-places input {
  background-color: #fafbfc;
  border-color: #dfe1e6;
  border-radius: 3px;
  border-width: 2px;
  border-style: solid;
  box-sizing: border-box;
  color: #091e42;
  cursor: text;
  padding: 8px 6px;
  font-size: 14px;
  margin-bottom: 10px;
}

.algolia-places input:focus {
  background: #fff;
}

.ap-name {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: baseline;
  height: auto;
}

.ap-suggestion {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: baseline;
  height: auto;
}

li + li {
  margin-top: 0px;
}
`;

export default GlobalStyle;
