import { gql } from "@apollo/client";

export const USER_FRAGMENT = gql`
  fragment userFragment on User {
    id
    email
    name
    createdAt
  }
`;

export const OFFER_FRAGMENT = gql`
  fragment offerFragment on Offer {
    id
    ownerId
    title
    slug
    lat
    lng
    radius
    owner {
      ...userFragment
    }
  }
  ${USER_FRAGMENT}
`;
