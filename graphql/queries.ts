import { gql } from "@apollo/client";
import { OFFER_FRAGMENT, USER_FRAGMENT } from "./fragments";

export const POSTS = gql`
  query users {
    users {
      ...userFragment
    }
  }
  ${USER_FRAGMENT}
`;

export const OFFERS = gql`
  query offers {
    offers {
      ...offerFragment
    }
  }
  ${OFFER_FRAGMENT}
`;

export const OFFER = gql`
  query offer($offerId: Int!) {
    offer(offerId: $offerId) {
      ...offerFragment
    }
  }
  ${OFFER_FRAGMENT}
`;
