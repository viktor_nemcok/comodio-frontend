import Link from "next/link";
import { useUsersQuery } from "../../types/apollo";
import Page from "../../components/Page";

const UsersPage = () => {
  const { data, loading, error } = useUsersQuery();

  if (loading || error) return <div>loading nebo error</div>;

  return (
    <Page seoTitle="Users List">
      <h1>Users List</h1>
      <p>Fetching na straně klienta.</p>
      <p>
        Využívá se vždy, když je k němu potřeba interakce, jinak používejme
        static props
      </p>
      <ul>
        {data?.users.map((user) => (
          <li key={user.id}>
            <ul>
              <li>{user.createdAt}</li>
              <li>{user.email}</li>
              <li>{user.name}</li>
            </ul>
          </li>
        ))}
      </ul>
      <p>
        <Link href="/">
          <a>Go home</a>
        </Link>
      </p>
    </Page>
  );
};

export default UsersPage;
