import Link from "next/link";
import { useOffersQuery } from "../../types/apollo";
import Page from "../../components/Page";

const UsersPage = () => {
  const { data, loading, error } = useOffersQuery();

  if (loading || error) return <div>loading nebo error</div>;

  return (
    <Page seoTitle="Offers">
      <h1>Offers List</h1>
      <p>Fetching na straně klienta.</p>
      <p>
        Využívá se vždy, když je k němu potřeba interakce, jinak používejme
        static props
      </p>
      <ul>
        {data?.offers.map((offer) => (
          <li key={offer.id}>
            <ul>
              <li>{offer.owner.name}</li>
              <li>{offer.id}</li>
              <li>
                <Link
                  as={`/offers/${offer.id}`}
                  href={`/offers/[offerId]`}
                  passHref
                >
                  <a>{offer.title}</a>
                </Link>
              </li>
            </ul>
          </li>
        ))}
      </ul>
      <p>
        <Link href="/">
          <a>Go home</a>
        </Link>
      </p>
    </Page>
  );
};

export default UsersPage;
