import Link from "next/link";
import { client } from "../../app/ApolloClient";
import styled from "../../app/styled";
import { OFFER, OFFERS } from "../../graphql/queries";
import { Offer } from "../../types/apollo";
import Page from "../../components/Page";

export async function getStaticPaths() {
  const res = await client.query({ query: OFFERS });
  const offers = res.data.offers;

  const paths = offers.map((offer: Offer) => ({
    params: { offerId: offer.id.toString() },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({
  params,
}: {
  params: { offerId: string };
}) {
  const res = await client.query({
    query: OFFER,
    variables: { offerId: parseInt(params.offerId) },
  });
  const offer = res.data.offer;

  return { props: { offer } };
}

const UsersPage = ({ offer }: { offer: Offer }) => {
  return (
    <Page seoTitle="Users List | Next.js + TypeScript Example">
      <FancyHeading>Users List</FancyHeading>
      <FancyText>Fetching na straně klienta.</FancyText>
      <ul>
        <li>{offer?.id}</li>
        <li>{offer?.lat}</li>
        <li>{offer?.lng}</li>
        <li>{offer?.radius}</li>
        <li>{offer?.ownerId}</li>
        <li>{offer?.slug}</li>
        <li>{offer?.title}</li>
      </ul>
      <p>Vlastník:</p>
      <ul>
        <li>{offer?.owner.email}</li>
        <li>{offer?.owner.name}</li>
        <li>{offer?.owner.createdAt}</li>
      </ul>
      <p>
        <Link href="/">
          <a>Go home</a>
        </Link>
      </p>
    </Page>
  );
};

const FancyText = styled.p`
  font-weight: 700;
  font-size: ${({ theme }) => theme.fontSize.big};
  color: ${({ theme }) => theme.colors.green};
`;

const FancyHeading = styled(FancyText)`
  font-size: ${({ theme }) => theme.fontSize.max};
`;

export default UsersPage;
