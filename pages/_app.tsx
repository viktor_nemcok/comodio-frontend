import { ApolloProvider } from '@apollo/client';
import App from 'next/app';
import React from 'react';
import { client } from '../app/ApolloClient';
import { ThemeProvider } from '../app/styled';
import { theme } from '../app/theme';
import NextNprogress from 'nextjs-progressbar';
import GlobalStyle from '../app/GlobalStyle';

class MyApp extends App {

    render() {
        const { Component, pageProps } = this.props;
        return (
            <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
            <NextNprogress
              color="#5da888"
              options={{ showSpinner: false }}
              startPosition={0.4}
              stopDelayMs={400}
              height={3}
            />
            <GlobalStyle />
            <Component {...pageProps} />
        </ThemeProvider>
      </ApolloProvider>
        )
    }
}

export default MyApp;