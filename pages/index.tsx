import Link from "next/link";
import Text from "../components/Text";
import Page from "../components/Page";
import styled from "../app/styled";


const IndexPage = () => (
  <Page seoTitle="Index" >
    <Heading>Úvodní stránka!!!</Heading>
    <Link href="/about" passHref>
      <a>
        <Text>Přejít na stránku ABOUT</Text>
      </a>
    </Link>
  </Page>
);


const Heading = styled(Text)`
  font-size: ${props => props.theme.fontSize.big};
  color: ${props => props.theme.colors.blue};
`;

export default IndexPage;
