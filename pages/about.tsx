import Link from 'next/link'
import Text from '../components/Text'
import Page from '../components/Page'

const AboutPage = () => (
  <Page seoTitle="About | Next.js + TypeScript Example">
    <Text>About</Text>
    <Text color="green" fontSize="medium">This is the about page</Text>
    <Text>
      <Link href="/">
        <a>Go home</a>
      </Link>
    </Text>
  </Page>
)

export default AboutPage
