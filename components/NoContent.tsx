import styled from '../app/styled';
import Box from './Box';
import Text from './Text';

const NoContent = (props: {
  title?: string;
  description: string;
  isScreenHeight?: boolean;
  children?: any;
}) => (
  <Wrapper isScreenHeight={props.isScreenHeight}>
      <Icon src="https://img.icons8.com/ios/344/home--v4.png" />
    {props.title && <Title>{props.title}</Title>}
    <Description>{props.description}</Description>
    {props.children}
  </Wrapper>
);

const Wrapper = styled(Box)<{ isScreenHeight?: boolean }>`
  justify-content: center;
  align-items: center;
  padding: ${props => props.theme.spacing.extreme} 0;
  height: ${props => (props.isScreenHeight ? `calc(100vh - ${props.theme.header.desktopHeight}px)` : 'auto')};
`;

const Title = styled(Text)`
  font-size: ${props => props.theme.fontSize.max};
  color: ${props => props.theme.colors.pink};
  padding: ${props => props.theme.spacing.small} 0;

  @media only screen and (max-width: 800px) {
    font-size: ${props => props.theme.fontSize.bigger};
    padding: ${props => props.theme.spacing.mini} 0;
  }
`;

const Description = styled(Text)`
  font-size: ${props => props.theme.fontSize.medium};
  color: ${props => props.theme.colors.green};
  margin-bottom: ${props => props.theme.spacing.default};

  @media only screen and (max-width: 800px) {
    font-size: ${props => props.theme.fontSize.normal};
  }
`;

const Icon = styled.img`
  width: 200px !important;
  height: 200px !important;

  @media only screen and (max-width: 800px) {
    width: 100px !important;
    height: 100px !important;
  }
`;

export default NoContent;
