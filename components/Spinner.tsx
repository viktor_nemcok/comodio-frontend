import { bounceIn } from "react-animations";
import styled, { keyframes } from "../app/styled";
import Box from "./Box";

type Props = { size?: number; height?: number };

const Spinner = ({ size = 200, height = 400 }: Props) => (
  <BouncyDiv height={height}>
    <img src="https://img.icons8.com/ios/344/home--v4.png" height={size} />
  </BouncyDiv>
);

export const HoverSpinner = () => (
  <HoverBox>
    <BouncyDiv height={400}>
      <img src="https://img.icons8.com/ios/344/home--v4.png" height={200} />
    </BouncyDiv>
  </HoverBox>
);

const HoverBox = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  z-index: 2;
  background: ${({ theme }) => theme.colors.lightPink};
`;

const BouncyDiv = styled(Box)<{ height: number }>`
  position: relative;
  top: 0;
  animation: 1s ${keyframes`${bounceIn}`} infinite;
  width: 100%;
  height: ${(props) => props.height}px;
  justify-content: center;
  align-items: center;
`;

export default Spinner;
