import Link from "next/link";
import { useState } from "react";
import styled from "../app/styled";
import { colors } from "../app/theme";
import Box from "./Box";
import Text from "./Text";

const Header = (props: { isTransparent?: boolean }) => {
  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [isShownSubmenu, setIsShownSubmenu] = useState(false);

  return (
    <Container>
      <Menu>
        <Logo>
          <Link passHref href="/">
            <NavItem as="a" noMargin>
              <LogoImg
                alt="Logo Svatba.cz"
                src={`https://img.icons8.com/ios/344/home--v4.png`}
              />
            </NavItem>
          </Link>
        </Logo>
        <Items isMobileMenuOpen={isMobileMenuOpen}>
          <Item>
            <Link passHref href="/offers">
              <NavItem as="a" isTransparent={props.isTransparent}>
                Nabídky
              </NavItem>
            </Link>
          </Item>
          <Item>
            <Link passHref href="/users">
              <NavItem as="a" isTransparent={props.isTransparent}>
                Uživatelé
              </NavItem>
            </Link>
          </Item>
          <Item>
            <NavItem
              color="pink"
              onMouseOver={() => setIsShownSubmenu(true)}
              onClick={() =>
                setIsShownSubmenu(isMobileMenuOpen ? true : !isShownSubmenu)
              }
              isFlex
            >
              Najeď na menu
            </NavItem>
            <Submenu
              onMouseOver={() => setIsShownSubmenu(true)}
              onMouseOut={() =>
                setIsShownSubmenu(isMobileMenuOpen ? true : false)
              }
              isShown={isMobileMenuOpen ? true : isShownSubmenu}
            >
              <Item>
                <Link passHref href="/profil/zpravy">
                  <NavItem
                    as="a"
                    noMargin
                    style={{
                      alignItems: "baseline",
                    }}
                  >
                    Zprávy
                  </NavItem>
                </Link>
              </Item>

              <Item>
                <Link passHref href={`/nastaveni`}>
                  <NavItem noMargin>Nastavení profilu</NavItem>
                </Link>
              </Item>
              <Item>
                <Link passHref href="/profil/vouchery">
                  <NavItem noMargin>PROMO a Vouchery</NavItem>
                </Link>
              </Item>
              <Item>
                <Link passHref href="/profil/videopruvodce">
                  <NavItem noMargin>Videoprůvodce</NavItem>
                </Link>
              </Item>
              <Item>
                <a href="/odhlaseni">
                  <NavItem noMargin>Odhlásit se</NavItem>
                </a>
              </Item>
            </Submenu>
          </Item>
          <Item>
            <Link passHref href="/prihlaseni">
              <NavItem as="a" isTransparent={props.isTransparent}>
                Přihlásit se
              </NavItem>
            </Link>
          </Item>
          <Item>
            <Link passHref href="/registrace">
              <NavItem as="a" isTransparent={props.isTransparent}>
                Registrace
              </NavItem>
            </Link>
          </Item>
        </Items>
        <Toggle isOpen={isMobileMenuOpen}>
          <Box onClick={() => setMobileMenuOpen(!isMobileMenuOpen)}>
            <img src="https://img.icons8.com/windows/344/macos-close.png" />
          </Box>
        </Toggle>
      </Menu>
    </Container>
  );
};

const Menu = styled.ul`
  list-style-type: none;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.li`
  order: 0;
  padding: 10px;
`;

const Items = styled(Box)<{ isMobileMenuOpen: boolean }>`
  text-align: center;
  flex-direction: row;
  justify-content: flex-start;
  order: 3;
  align-items: center;

  @media only screen and (max-width: 850px) {
    background: ${(props) => props.theme.colors.white};
    position: fixed;
    overflow: scroll;
    top: 0;
    left: 0;
    width: ${(props) => (props.isMobileMenuOpen ? "80%" : "100%")};
    height: 100vh;
    flex-direction: column;
    align-items: center;
    transform: ${(props) =>
      props.isMobileMenuOpen ? "translateX(25%)" : "translateX(100%)"};
    transition: transform 0.3s ease-in-out;
    box-shadow: 0 0px 12px 6px hsla(0, 0%, 0%, 0.5);
  }
`;

const Item = styled.li`
  position: relative;
  display: block;
  width: auto;
  align-items: center;

  @media only screen and (max-width: 850px) {
    width: 100%;
  }
`;

const Submenu = styled.ul<{ isShown?: boolean }>`
  display: ${(props) => (props.isShown ? "block" : "none")};
  list-style-type: none;
  background: ${(props) => props.theme.colors.white};
  z-index: 10;

  @media only screen and (min-width: 851px) {
    position: absolute;
    top: ${(props) => props.theme.spacing.big};
    right: 25%;
    padding: 0 ${(props) => props.theme.spacing.default};
    box-shadow: 0px 4px 24px 0px hsla(0, 0%, 0%, 0.1);
    border-radius: 4px;
    width: fit-content;
    &::after {
      content: "";
      position: absolute;
      width: 15px;
      height: 15px;
      top: 0px;
      right: 50%;
      transform: translate(50%, -50%) rotate(45deg);
      background: ${(props) => props.theme.colors.white};
    }

    & > ${Item} {
      padding: ${({ theme }) => theme.spacing.small};
      align-items: flex-start;
      text-align: left;
    }

    & > ${Item}:not(:last-child) {
      border-bottom: 1px solid ${({ theme }) => theme.colors.lightGrey};
    }
  }

  @media only screen and (max-width: 850px) {
    position: relative;
  }
`;

const Toggle = styled.li<{ isOpen?: boolean }>`
  padding: ${(props) => props.theme.spacing.default};
  position: ${(props) => (props.isOpen ? "fixed" : "relative")};
  right: ${({ theme }) => theme.spacing.default};
  display: none;
  width: auto;
  order: 1;

  @media only screen and (max-width: 850px) {
    display: block;
  }
`;

const Container = styled(Box)<{ isTransparent?: boolean }>`
  position: ${(props) => (props.isTransparent ? "absolute" : "relative")};
  top: 0;
  z-index: 100;
  padding: ${({ theme }) => theme.spacing.default};
  width: 100%;
  background: ${(props) =>
    props.isTransparent ? "transparent" : props.theme.colors.white};
`;

const NavItem = styled(Text)<{
  noMargin?: boolean;
  isTransparent?: boolean;
  color?: keyof typeof colors;
  isFlex?: boolean;
}>`
  cursor: pointer;
  color: ${(props) =>
    props.color
      ? props.color
      : props.isTransparent
      ? props.theme.colors.white
      : props.theme.colors.black};
  text-transform: uppercase;
  transition: all 300ms;
  display: ${(props) => (props.isFlex ? "flex" : "block")};
  align-items: baseline;
  justify-content: center;
  margin-left: ${(props) => (props.noMargin ? 0 : props.theme.spacing.big)};

  @media only screen and (max-width: 850px) {
    color: ${({ theme }) => theme.colors.black};
    display: ${(props) => (props.isFlex ? "flex" : "block")};
    padding: ${(props) => props.theme.spacing.default};
    margin-left: 0;
  }

  &:hover {
    text-decoration: underline solid ${({ theme }) => theme.colors.pink};
    color: ${({ theme }) => theme.colors.green};
  }
`;

const LogoImg = styled.img`
  height: 30px;
  position: relative;
  top: -5px;
`;

export default Header;
