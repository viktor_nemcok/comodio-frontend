import styled from "../app/styled";
import { colors } from "../app/theme";
import Text from "./Text";

// eslint-disable-next-line
const Button = styled(Text).attrs(() => ({ as: "button" }))<{
  color: keyof typeof colors;
  isOutline?: boolean;
  isTextInverted?: boolean;
  isFullWidth?: boolean;
  fontWeight?: string;
  withTopMargin?: boolean;
  withRightMargin?: boolean;
  withBottomMargin?: boolean;
  withLeftMargin?: boolean;
  isSmall?: boolean;
}>`
  background: ${(props) =>
    props.isOutline ? "transparent" : colors[props.color]};
  border: 1px ${(props) => colors[props.color]} solid;
  border-radius: ${({ theme }) => theme.border.radius}px;
  color: ${(props) =>
    props.isOutline
      ? colors[props.color]
      : props.isTextInverted
      ? ({ theme }) => theme.colors.black
      : ({ theme }) => theme.colors.white};
  text-decoration: none !important;
  padding: ${({ theme, isSmall }) =>
      isSmall ? theme.spacing.mini : theme.spacing.small}
    ${({ theme, isSmall }) =>
      isSmall ? theme.spacing.small : theme.spacing.default};
  cursor: pointer;
  height: auto;
  width: ${(props) => (props.isFullWidth ? "auto" : "fit-content")};
  justify-content: space-evenly;
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : "600")};
  transition: all 300ms;
  box-shadow: 0 1px 5px 0 hsla(0, 0%, 0%, 0.2);
  margin-top: ${(props) =>
    props.withTopMargin ? ({ theme }) => theme.spacing.small : 0};
  margin-right: ${(props) =>
    props.withRightMargin ? ({ theme }) => theme.spacing.small : 0};
  margin-bottom: ${(props) =>
    props.withBottomMargin ? ({ theme }) => theme.spacing.small : 0};
  margin-left: ${(props) =>
    props.withLeftMargin ? ({ theme }) => theme.spacing.small : 0};
  font-size: ${({ isSmall, theme }) =>
    isSmall ? theme.fontSize.small : theme.fontSize.normal};

  &:hover {
    background-color: ${({ theme }) => theme.colors.white};
    color: ${(props) =>
      props.isOutline
        ? props.isTextInverted
          ? ({ theme }) => theme.colors.darkGrey
          : ({ theme }) => theme.colors.black
        : props.isTextInverted
        ? ({ theme }) => theme.colors.darkGrey
        : colors[props.color]};
  }
`;

export default Button;
