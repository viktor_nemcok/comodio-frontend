import Head from "next/head";
import styled from "../app/styled";
import Box from "./Box";
import Footer from "./Footer";
import Header from "./Header";
import NoContent from "./NoContent";
import Spinner from "./Spinner";

type Props = {
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;
  seoKeywords?: string[];
  seoNoIndex?: boolean;
  isTransparent?: boolean;
  children?: any;
  underFooterChildren?: any;
  isLoading?: boolean;
  isError?: boolean;
  hideFooter?: boolean;
  itemScope?: boolean;
  itemType?: string;
};

const defaultDescription = "Profi agregátor nemovitostí!";
const defaultImage = "";

const Page = ({
  isTransparent,
  seoTitle,
  seoDescription,
  seoKeywords,
  seoImage,
  seoNoIndex,
  children,
  underFooterChildren,
  isLoading,
  isError,
  hideFooter,
  itemScope,
  itemType,
}: Props) => {
  return (
    <Flex>
      <Head>
        <meta charSet="utf-8" />
        <title>{seoTitle ? seoTitle + " | Comodio" : "Comodio"}</title>
        <meta
          name="description"
          content={seoDescription || defaultDescription}
        />
        <meta property="og:title" content={seoTitle || "Comodio"} />
        <meta
          property="og:description"
          content={seoDescription || defaultDescription}
        />
        <meta property="og:type" content="website" />
        <meta property="og:image" content={seoImage || defaultImage} />
        {seoKeywords && seoKeywords.length > 0 ? (
          <meta name="keywords" content={seoKeywords.join(",")} />
        ) : undefined}
        <meta name="robots" content={seoNoIndex ? "noindex" : "all"} />
        <script type="text/javascript" src="/assets/scripts/seznam.js" />
        <script type="text/javascript" src="/assets/scripts/smartlook.js" />
        <script type="text/javascript" src="//c.imedia.cz/js/retargeting.js" />
      </Head>
      <Box>
        <Header isTransparent={isTransparent} />
        <Container itemScope={itemScope} itemType={itemType}>
          <>
            {isLoading && !isError && <Spinner />}
            {isError && (
              <NoContent
                title="Vyskytla se chyba"
                description="Obsah, který hledáte se nám nepodařilo načíst."
                isScreenHeight
              />
            )}

            {!isError && children}
          </>
        </Container>
      </Box>
      {!hideFooter && !isLoading && <Footer />}
      {underFooterChildren}
    </Flex>
  );
};

export default Page;

const Flex = styled(Box)`
  min-height: 100vh;
  justify-content: space-between;
`;

const Container = styled(Box)`
  width: 100%;
  position: relative;
  margin: 0 auto;
`;
