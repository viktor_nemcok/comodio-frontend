import moment from 'moment';
import Link from 'next/link';
import styled from '../app/styled';
import Box from './Box';
import Text from './Text';
import Button from './Button';

const Footer = () => {

  return (
    <Container>
      <Column>
        <MediumText>Sledujte nás</MediumText>
        <BiggerText>@comodio</BiggerText>
      </Column>
      <Column>
        <Link href={`/podminky-uziti`}>
          <a>
            <UpperText>Podmínky užití</UpperText>
          </a>
        </Link>
        <Link href={`/platby`}>
          <a>
            <UpperText>Opakované platby</UpperText>
          </a>
        </Link>
        <Link href={`/gdpr`}>
          <a>
            <UpperText>GDPR</UpperText>
          </a>
        </Link>
        <Link href={`/obchodni-podminky`}>
          <a target={'_blank'}>
            <UpperText>Obchodní podmínky</UpperText>
          </a>
        </Link>
        <Link href={'/podpora'}>
          <a>
            <UpperText>Podpora</UpperText>
          </a>
        </Link>
      </Column>
      <Column>
        <MediumText>Máte zájem se prezentovat na COMODIO?</MediumText>
        <Link href="/registrace-dodavatel" passHref>
          <a>
            <Button color="pink">Více informací</Button>
          </a>
        </Link>

        <Credentials>
          &copy;
          {moment()
            .format('YYYY')
            .toString()}{' '}
          Comodio s.r.o. Všechna práva vyhrazena
        </Credentials>
      </Column>
    </Container>
  );
};

const Container = styled(Box)`
  flex-direction: row;
  position: relative;
  bottom: 0;
  justify-content: space-around;
  background: ${({ theme }) => theme.colors.lightGrey};
  padding: ${({ theme }) => theme.spacing.default};

  @media only screen and (max-width: 768px) {
    flex-direction: column;
    height: 100%;
  }
`;

const Column = styled(Box)`
  align-items: center;
  padding: ${({ theme }) => theme.spacing.default};
  justify-content: space-around;
  &:nth-child(3) {
    align-items: flex-end;
    text-transform: none;
    font-family: ${({ theme }) => theme.text.fontFamilySecondary};
    justify-content: space-between;

    @media only screen and (max-width: 768px) {
      align-items: center;
    }
  }
`;

const MediumText = styled(Text)`
  color: ${({ theme }) => theme.colors.black};
  font-size: ${({ theme }) => theme.fontSize.medium};
  font-family: ${({ theme }) => theme.text.fontFamilySecondary};

  @media only screen and (max-width: 500px) {
    text-align: center;
  }
`;

const BiggerText = styled(MediumText)`
  font-size: ${({ theme }) => theme.fontSize.bigger};
  padding-bottom: ${({ theme }) => theme.spacing.default};
`;

const UpperText = styled(Text)`
  text-transform: uppercase;
  cursor: pointer;

  &:hover {
    text-decoration: underline solid ${({ theme }) => theme.colors.pink};
  }
`;

const Credentials = styled(Text)`
  @media only screen and (max-width: 500px) {
    text-align: center;
  }
`;

export default Footer;
